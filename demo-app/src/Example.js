import React, { useState, useEffect} from 'react';

import {useStateValue} from './state';
import './Example.css';



function Example(props) {

    const [count, setCount] = useState(0);
    const [refresh, doRefresh] = useState(true);

    const [{multiply}, dispatch] = useStateValue();

    useEffect(() => {
        setCount(0);
    }, [refresh]);

    return (

        <div>
            <h1 className="text">Example Component</h1>
            <br/>
            <p className="text">current Value is: {count}</p>
            <button
                id="button1"
                onClick={() => {
                    setCount(count + 1);
                    dispatch({
                        type: 'multiply',
                        multiply: {multiply: count + 1}
                    })
                }}
                className="text"
            >
                +
            </button>
            <button
                onClick={() => {
                    setCount(count - 1);
                    dispatch({
                        type: 'divide',
                        multiply: {multiply: count -1}
                    })
                }}> 
                -
            </button>
            <br/>
            <button
                onClick={() => {
                    doRefresh(!refresh);
                    dispatch({
                        type: 'refresh'
                    })
                }}
                className="text"
            >
                Refresh
            </button>
            <br/>
            <p className="text" id='multiplied'>Multiplied value: {multiply.multiply}</p>
        </div>
    );
}

export default Example;
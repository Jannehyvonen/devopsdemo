import React from 'react';

import {StateProvider} from './state';
import MultiplyReducer from './reducers/multiply/multiplyReducer';

import Example from './Example'
import Example2 from './Example2'

function App() {

  const initialState = {multiply: {multiply: 0}};

  return (
      <div>
        <StateProvider initialState={initialState} reducer={MultiplyReducer}>
          <Example/>
          <Example2 />
        </StateProvider>
      </div>
  );
}

export default App;

import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import Example from './Example';
import Example2 from './Example2';
import {StateProvider} from './state';
import MultiplyReducer from './reducers/multiply/multiplyReducer';

let container;

beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
});

afterEach(() => {
    document.body.removeChild(container);
    container = null;
});

it('Can render and show value from context', () => {
    const initialState = {multiply: {multiply: 0}};
    act(() => {
        ReactDOM.render((
            <StateProvider initialState={initialState} reducer={MultiplyReducer}>
                <Example />
                <Example2 />
            </StateProvider>
        ), container);
    });
    let label = document.querySelector('#value');
    expect(label.textContent).toBe('Multiplied value in component Example 2: 0');

    const button = document.querySelector('#button1');
    act(() => {
        button.dispatchEvent(new MouseEvent('click', {bubbles: true}));
    });
    expect(label.textContent).toBe('Multiplied value in component Example 2: 10');
});
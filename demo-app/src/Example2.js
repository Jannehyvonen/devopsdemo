import React from 'react';

import {useStateValue} from './state';
import './Example.css';



function Example2(props) {

    const [{multiply}] = useStateValue();

    return (
        <div>
             <p
                className="text"
                id="value"
                >Multiplied value in component Example 2: {multiply.multiply}</p>
        </div>
    );
}export default Example2;


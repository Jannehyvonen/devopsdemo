function Multiply(state, action) {

    let newState = {};

    switch(action.type) {
        case 'multiply':
            newState = {...state};
            newState.multiply.multiply = action.multiply.multiply * 10;
            return newState;
        case 'divide':
            newState = {...state};
            newState.multiply.multiply = action.multiply.multiply * 10;
            return newState;
        case 'refresh':
            newState = {...state};
            newState.multiply.multiply = 0;
            return newState;
        default:
            return state;
    }
};

export default Multiply;
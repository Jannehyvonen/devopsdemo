import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import Example from './Example';
import {useStateValue} from './state';
import {StateProvider} from './state';
import MultiplyReducer from './reducers/multiply/multiplyReducer';

let container;

beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
});

afterEach(() => {
    document.body.removeChild(container);
    container = null;
});

it('Can render and update multiply count', () => {
    const initialState = {multiply: {multiply: 0}};
    act(() => {
        ReactDOM.render((
            <StateProvider initialState={initialState} reducer={MultiplyReducer}>
                <Example />
            </StateProvider>
        ), container);
    });
    let label = container.querySelector('#multiplied');
    const button = container.querySelector('#button1');
    expect(label.textContent).toBe('Multiplied value: 0');

    act(() => {
        button.dispatchEvent(new MouseEvent('click', {bubbles: true}));
    });
    label = container.querySelector('#multiplied');
    expect(label.textContent).toBe('Multiplied value: 10');
});